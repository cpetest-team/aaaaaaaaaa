package com.service.teqewqeq.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-06-01T01:57:03.226Z")

@RestSchema(schemaId = "teqewqeq")
@RequestMapping(path = "/teqewqeq", produces = MediaType.APPLICATION_JSON)
public class TeqewqeqImpl {

    @Autowired
    private TeqewqeqDelegate userTeqewqeqDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userTeqewqeqDelegate.helloworld(name);
    }

}
